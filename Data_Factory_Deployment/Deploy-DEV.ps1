﻿##################################################################################################

# ADF Deployment script
#
# Press f5 to execute entire script
# Press f8 to execute highlighted script
#
# This script will deploy all files in the LinkedServices, Datasets and Pipelines folders to an Azure
# Data Factory instance. If the files already exist, they will be overwritten. If the Resource Group and/or 
# Data Factory instance does not already exist, it will be created.
#
# A valid Azure login is required prior to execution.
#
# Azure PowerShell is required to execute this script.  Download/install instructions can be found at: 
#
#      https://docs.microsoft.com/en-us/powershell/azure/install-azurerm-ps?view=azurermps-5.6.0
#

#################################################################################################


#=======================================#
#              Parameters               #
#=======================================#

$AzureSubscription = "Microsoft Azure Enterprise"
$ResourceGroupName = "RG-DataServices-DEV"
$DataFactoryName = "df-dataservices-etl-dev"
$DataFactoryLocation = "EastUS"
$IntegrationRuntimeName = "AutoResolveIntegrationRuntime"
$IntegrationRuntimeLocation = "eastus"
$StorageAccountName = 'sadataservicesdev'
$containterName = 'config-files'


#=======================================#
#     Script Execution Begins Here      #
#=======================================#

$LinkedServicesFilePath = ".\LinkedServices\LinkedServicesall"
$DatasetsFilePath = ".\Datasets"
$PipelinesFilePath = ".\Pipelines"
$ConfigFilePath = ".\config-files\configfilesall"
$MasterConfigFilePath = ".\config-files\configfilesDEV"
$TriggerFilePath = ".\Triggers"
$KVLinkedServicesFilePath = ".\LinkedServices\LinkedServicesKeyVaultDEV"

$ErrorActionPreference = "SilentlyContinue"
$ErrorFlag = 0

try {
    $login = Connect-AzAccount
    if ([string]::IsNullOrEmpty($login)) {
        Write-Host ""
        Write-Host "Invalid Azure Login"
        Write-Host ""
        Write-Host -NoNewLine 'Press any key to continue...'
        $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown')
        $ErrorFlag = 1
    }
}
catch {
    Write-Host ""
    Write-Host "Error connecting to Azure. Please try again"
    Write-Host ""
    Write-Host -NoNewLine 'Press any key to continue...'
    $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown')
    $ErrorFlag = 1
}


if ($ErrorFlag -eq 0)
{

    Select-AzSubscription -SubscriptionId $AzureSubscription         

    try {
        # Create Resource Group if it does not exist
        $ResourceGroup = Get-AzResourceGroup -Name $ResourceGroupName
        if (!$ResourceGroup) {
            Write-Host "Creating Resource Group..." -ForegroundColor "Yellow"
            Set-AzResourceGroup -Name $ResourceGroupName
        }
        else {
            Write-Host "Resource Group Already Exists" -ForegroundColor "Green"
        }
    }
    catch {
        Write-Host ""
        Write-Host "Error Creating Resource Group"
        Write-Host ""
        Write-Host -NoNewLine 'Press any key to continue...'
        $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown')
        $ErrorFlag = 1
    }

}

if ($ErrorFlag -eq 0) {

    try {
        # Create Data Factory instance if it does not exist
        $DataFactory = Get-AzDataFactoryV2 -ResourceGroupName $ResourceGroupName -Name $DataFactoryName
        if (!$DataFactory) {
            Write-Host "Creating Data Factory Instance..." -ForegroundColor "Yellow"
            Set-AzDataFactoryV2 -ResourceGroupName $ResourceGroupName -Name $DataFactoryName -Location $DataFactoryLocation
        }
        else {
            Write-Host "Data Factory Instance already exists" -ForegroundColor "Green"
        }
    }
    catch {
        Write-Host ""
        Write-Host "Error Creating Data Factory Instance"
        Write-Host ""
        Write-Host -NoNewLine 'Press any key to continue...'
        $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown')
        $ErrorFlag = 1
    }

}


if ($ErrorFlag -eq 0) {
	
    $LinkedServices = Get-ChildItem -Path $LinkedServicesFilePath
    $Datasets = Get-ChildItem -Path $DatasetsFilePath
    $Pipelines = Get-ChildItem -Path $PipelinesFilePath
    $ConfigFiles = Get-ChildItem -Path $ConfigFilePath
    $MasterConfigFiles = Get-ChildItem -Path $MasterConfigFilePath
    $Triggers = Get-ChildItem -Path $TriggerFilePath
    $KVLinkedServices = Get-ChildItem -Path $KVLinkedServicesFilePath
    $StorageAccount = Get-AzStorageAccount -ResourceGroupName $ResourceGroupName -Name $StorageAccountName

    # Create LinkedServices
    Write-Host "Creating LinkedServices..." -ForegroundColor "Yellow"
    foreach($LinkedService in $LinkedServices) {
    	Set-AzDataFactoryV2LinkedService -DataFactoryName $dataFactoryName -ResourceGroupName $ResourceGroupName -Name $LinkedService.Name.Split('.')[0] -File ($linkedServicesFilePath + '\' + $LinkedService.Name)
    }

    # Create Azure Key Vault LinkedService
    # This linked service is unique per environment
    Write-Host "Creating Key Vault LinkedServices..." -ForegroundColor "Yellow"
    foreach($KVLinkedService in $KVLinkedServices) {
    	Set-AzDataFactoryV2LinkedService -DataFactoryName $dataFactoryName -ResourceGroupName $ResourceGroupName -Name $KVLinkedService.Name.Split('.')[0] -File ($KVLinkedServicesFilePath + '\' + $KVLinkedService.Name)
    }

    # Create Datasets
    Write-Host "Creating Datasets..." -ForegroundColor "Yellow"
    foreach($Dataset in $Datasets) {
        Set-AzDataFactoryV2Dataset -DataFactoryName $dataFactoryName -ResourceGroupName $resourceGroupName -Name $Dataset.Name.Split('.')[0] -File ($datasetsFilePath + '\' + $Dataset.Name)
    }
    
    # Create Pipeline
    Write-Host "Creating Pipelines..." -ForegroundColor "Yellow"
    foreach($Pipeline in $Pipelines) {
	    Set-AzDataFactoryV2Pipeline -DataFactoryName $dataFactoryName -ResourceGroupName $resourceGroupName -Name $Pipeline.Name.Split('.')[0] -File ($pipelinesFilePath + '\' + $Pipeline.Name)}
    
    # Upload blobs to storage account
    Write-Host "Uploading config files..." -ForegroundColor "Yellow"
    foreach($ConfigFile in $ConfigFiles) {
    	Set-AzStorageBlobContent -Container $containterName -Blob $ConfigFile -File ($ConfigFilePath + '\' + $ConfigFile.Name) -Context $StorageAccount.Context
    }

    # Upload Master config blobs to storage account
    # Master config files are unique per environment
    Write-Host "Uploading Master config files..." -ForegroundColor "Yellow"
    foreach($MasterConfigFile in $MasterConfigFiles) {
    	Set-AzStorageBlobContent -Container $containterName -Blob $MasterConfigFile -File ($MasterConfigFilePath + '\' + $MasterConfigFile.Name) -Context $StorageAccount.Context
    }

    #Create Triggers
    Write-Host "Creating Triggers..." -ForegroundColor "Yellow"
    foreach($Trigger in $Triggers) {
    	Set-AZDataFactoryV2Trigger -DataFactoryName $dataFactoryName -ResourceGroupName $resourceGroupName -Name $Trigger.Name.Split('.')[0] -DefinitionFile ($TriggerFilePath + '\' + $Trigger.Name)
    }

    Write-Host "Deployment Complete" -ForegroundColor "Green"
    Write-Host "" 
    Write-Host "Press any key to continue..."
    $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown')
}

if ($ErrorFlag -eq 1) {
	Write-Host ""
        Write-Host "Error. Deployement Failed" -ForegroundColor "Red"
        Write-Host ""
        Write-Host -NoNewLine 'Press any key to continue...'
        $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown')
}
